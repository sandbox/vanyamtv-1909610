Taxonomy Tracking
---------------------------

Description
-----------
The Taxonomy Tracking module provides functionality for taxonomy tracking.

Requirements
------------
1. Drupal 7.x core - version 7.4 or later
   (http://drupal.org/project/drupal).
2. Taxonomy 7.x core module - version 7.4 or later
   (http://drupal.org/project/drupal).

Optional Requirements
------------
1. CiviCRM 4.x contributed module - version 4.1 or later
   (http://drupal.org/project/civicrm, http://civicrm.org).
   
Installation
------------
1. Put the module in your drupal modules directory 
   (sites/all/modules/contrib).
2. Enable it at Modules page (admin/modules, package = "Taxonomy").
3. Configure the module at Taxonomy tracking settings page
   (admin/config/taxonomy-tracking).

Configuration
-------------
1. Configure the module at Taxonomy tracking settings page
   (admin/config/taxonomy-tracking).
  - Select vocabularies which should be tracked;
  - Define possible activity levels for user which will be used for
    calculating how often user visits tracked pages on the site;
  - Determine a frequency of data aggregation
    (there are 2 possible values available: every cronjob running or
     every 24 hours).
2. Configure user permissions at User Roles Permissions section
   (Administration > People > Permissions > taxonomy_tracking module,
    Administration > People > Permissions > CiviCRM,
    admin/people/permissions):
  - Set 'Administer taxonomy tracking configuration' user permission,
    which allows user to configure taxonomy tracking settings;
  - Set 'CiviCRM: access all custom data' user permission for anonymous user.
    This permission is mandatory and allows for cron to aggregate taxonomy data.

Usage
-----
1. Admin can select vocabularies terms of which will be tracked in the system.
   After selecting vocabularies and saving settings corresponding Custom group
   will be created in the CiviCRM (CiviCRM > Administer > Customize Data and
   Screens > Custom Fields, civicrm/admin/custom/group) and module starts to
   collect tracking data for selected vocabularies. 
2. Admin can enable/disable "User activity level" functionality and define
   possible user activity levels which apply to the calculation of user activity
   level (how often user visits tracked pages on the site). After enabling
   'User activity level' functionality in the CiviCRM corresponding Custom group
   will be created.
3. During each cronjob or every 24 hours collected taxonomy tracking data will
   be aggregated and moved to the corresponding Custom group in the CiviCRM.
4. When user delete a node/term/vocabulary all tracked data for such
   node/term/vocabulary will be deleted from the system (only from Drupal,
   CiviCRM will not be touched).
5. When admin delete a user all tracked data will be deleted from the system
   (from the Drupal and CiviCRM).
